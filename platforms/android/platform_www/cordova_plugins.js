cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-device/www/device.js",
        "id": "cordova-plugin-device.device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/phonegap-plugin-contentsync/www/index.js",
        "id": "phonegap-plugin-contentsync.ContentSync",
        "clobbers": [
            "window.ContentSync",
            "window.Zip",
            "window.zip"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.2.2",
    "cordova-plugin-device": "1.1.2",
    "cordova-plugin-console": "1.0.3",
    "phonegap-plugin-contentsync": "1.2.3"
};
// BOTTOM OF METADATA
});